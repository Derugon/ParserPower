<?php
/**
 * List Class
 *
 * @package   ParserPower
 * @author    Eyes <eyes@aeongarden.com>, Samuel Hilson <shilson@fandom.com>
 * @copyright Copyright � 2013 Eyes
 * @copyright 2019 Wikia Inc.
 * @license   GPL-2.0-or-later
 */

namespace ParserPower;

use Countable;
use Parser;
use PPFrame;

class ParserPowerLists {
	/**
	 * Flag for alphanumeric sorting. 0 as this is a default mode.
	 */
	const SORT_ALPHA = 0;

	/**
	 * Flag for numeric sorting.
	 */
	const SORT_NUMERIC = 4;

	/**
	 * Flag for case insensitive sorting. 0 as this is a default mode, and ignored in numeric sorts.
	 */
	const SORT_NCS = 0;

	/**
	 * Flag for case sensitive sorting. 0 as this is a default mode, and ignored in numeric sorts.
	 */
	const SORT_CS = 2;

	/**
	 * Flag for sorting in ascending order. 0 as this is a default mode.
	 */
	const SORT_ASC = 0;

	/**
	 * Flag for sorting in descending order.
	 */
	const SORT_DESC = 1;

	/**
	 * Flag for index search returning a positive index. 0 as this is a default mode.
	 */
	const INDEX_POS = 0;

	/**
	 * Flag for index search returning a negative index.
	 */
	const INDEX_NEG = 4;

	/**
	 * Flag for case insensitive index search. 0 as this is a default mode.
	 */
	const INDEX_NCS = 0;

	/**
	 * Flag for case sensitive index search.
	 */
	const INDEX_CS = 2;

	/**
	 * Flag for forward index search. 0 as this is a default mode.
	 */
	const INDEX_ASC = 0;

	/**
	 * Flag for reverse index search.
	 */
	const INDEX_DESC = 1;

	/**
	 * Flag for case insensitive item removal. 0 as this is a default mode.
	 */
	const REMOVE_NCS = 0;

	/**
	 * Flag for case sensitive item removal.
	 */
	const REMOVE_CS = 1;

	/**
	 * Registers the list handling parser functions with the parser.
	 *
	 * @param Parser $parser The parser object being initialized.
	 *
	 * @return void
	 */
	public static function setup(&$parser) {
		$parser->setFunctionHook(
			'lstcnt',
			'ParserPower\\ParserPowerLists::lstcntRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'lstsep',
			'ParserPower\\ParserPowerLists::lstsepRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'lstelem',
			'ParserPower\\ParserPowerLists::lstelemRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'lstsub',
			'ParserPower\\ParserPowerLists::lstsubRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'lstfnd',
			'ParserPower\\ParserPowerLists::lstfndRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'lstind',
			'ParserPower\\ParserPowerLists::lstindRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'lstapp',
			'ParserPower\\ParserPowerLists::lstappRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'lstprep',
			'ParserPower\\ParserPowerLists::lstprepRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'lstjoin',
			'ParserPower\\ParserPowerLists::lstjoinRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'lstcntuniq',
			'ParserPower\\ParserPowerLists::lstcntuniqRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'listunique',
			'ParserPower\\ParserPowerLists::listuniqueRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'lstuniq',
			'ParserPower\\ParserPowerLists::lstuniqRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'listfilter',
			'ParserPower\\ParserPowerLists::listfilterRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'lstfltr',
			'ParserPower\\ParserPowerLists::lstfltrRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'lstrm',
			'ParserPower\\ParserPowerLists::lstrmRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'listsort',
			'ParserPower\\ParserPowerLists::listsortRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'lstsrt',
			'ParserPower\\ParserPowerLists::lstsrtRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'listmap',
			'ParserPower\\ParserPowerLists::listmapRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'lstmap',
			'ParserPower\\ParserPowerLists::lstmapRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'lstmaptemp',
			'ParserPower\\ParserPowerLists::lstmaptempRender',
			SFH_OBJECT_ARGS
		);
		$parser->setFunctionHook(
			'listmerge',
			'ParserPower\\ParserPowerLists::listmergeRender',
			SFH_OBJECT_ARGS
		);
	}

	/**
	 * This function splits a string of delimited values into an array by a given delimiter or default delimiters.
	 *
	 * @param string $sep  The delimiter used to separate the strings, or an empty string to use the default delimiters.
	 * @param string $list The list in string format with values separated by the given or default delimiters.
	 *
	 * @return array The values in an array of strings.
	 */
	private static function explodeList($sep, $list) {
		if ($sep === '') {
			$values = preg_split('/(.)/u', $list, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
		} else {
			$values = explode($sep, $list);
		}

		return $values;
	}

	/**
	 * This function gets the specified element from the array after filtering out any empty values before it so that
	 * the empty values are skipped in index counting. The returned element is unescaped.
	 *
	 * @param array $inIndex  The 1-based index of the array element to get, or a negative value to start from the end.
	 * @param array $inValues The array to get the element from.
	 *
	 * @return string The array element, trimmed and with character escapes replaced, or empty string if not found.
	 */
	private static function arrayElementTrimUnescape($inIndex, $inValues) {
		if ($inIndex > 0) {
			$curOutIndex = 1;
			$count = (is_array($inValues) || $inValues instanceof Countable) ? count($inValues) : 0;
			for ($curInIndex = 0; $curInIndex < $count; ++$curInIndex) {
				$trimmedValue = trim($inValues[$curInIndex]);
				if (!ParserPower::isEmpty($trimmedValue)) {
					if ($inIndex === $curOutIndex) {
						return ParserPower::unescape($trimmedValue);
					} else {
						++$curOutIndex;
					}
				}
			}
		} elseif ($inIndex < 0) {
			$curOutIndex = -1;
			for ($curInIndex = count($inValues) - 1; $curInIndex > -1; --$curInIndex) {
				$trimmedValue = trim($inValues[$curInIndex]);
				if (!ParserPower::isEmpty($trimmedValue)) {
					if ($inIndex === $curOutIndex) {
						return ParserPower::unescape($trimmedValue);
					} else {
						--$curOutIndex;
					}
				}
			}
		}

		return '';
	}

	/**
	 * This function trims whitespace of each value from the array. It also performs un-escaping on each item.
	 *
	 * @param array $inValues The array to trim and unescape.
	 *
	 * @return array A new array with trimmed values and character escapes replaced.
	 */
	private static function arrayTrimUnescape($inValues) {
		$outValues = [];

		foreach ($inValues as $inValue) {
			$outValues[] = ParserPower::unescape(trim($inValue));
		}

		return $outValues;
	}

	/**
	 * This function trims whitespace each value while also filtering empty values from the array, then slicing it
	 * according to specified offset and length. It also performs un-escaping on each item. Note that values
	 * that are only empty after the unescape are preserved.
	 *
	 * @param int   $offset   The index of the first array element to get.
	 * @param int   $length   The number of array elements to get.
	 * @param array $inValues The array to trim, remove empty values from, slice, and unescape.
	 *
	 * @return array A new array with trimmed values, character escapes replaced, and empty values pre unescape removed.
	 */
	private static function arrayTrimSliceUnescape($offset, $length, $inValues) {
		$midValues = [];
		$outValues = [];

		foreach ($inValues as $inValue) {
			$trimmedValue = trim($inValue);
			if (!ParserPower::isEmpty($trimmedValue)) {
				$midValues[] = $trimmedValue;
			}
		}

		if ($offset > 0) {
			--$offset;
		}

		if ($length !== null) {
			$length = intval($length);
		} elseif ($offset < 0) {
			$length = -$offset;
		} else {
			$length = count($midValues) - $offset;
		}

		$midValues = array_slice($midValues, $offset, $length);
		foreach ($midValues as $midValue) {
			$outValues[] = ParserPower::unescape($midValue);
		}

		return $outValues;
	}

	/**
	 * This function trims whitespace of each value while also filter empty values from the array. It also
	 * performs un-escaping on each item. Note that values that are only empty after the unescape are preserved.
	 *
	 * @param array $inValues The array to trim, unescape, and remove empty values from.
	 *
	 * @return array A new array with trimmed values, character escapes replaced, and empty values preunescape removed.
	 */
	private static function arrayTrimFilterUnescape($inValues) {
		$outValues = [];

		foreach ($inValues as $inValue) {
			$trimmedValue = trim($inValue);
			if (!ParserPower::isEmpty($trimmedValue)) {
				$outValues[] = ParserPower::unescape($trimmedValue);
			}
		}

		return $outValues;
	}

	/**
	 * This function directs the counting operation for the lstcnt function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string The function output.
	 */
	public static function lstcntRender($parser, $frame, $params) {
		$list = isset($params[0]) ? trim($frame->expand($params[0])) : '';
		if ($list === '') {
			return '';
		}

		$sep = isset($params[1]) ? ParserPower::unescape(trim($frame->expand($params[1]))) : ',';

		$sep = $parser->mStripState->unstripNoWiki($sep);

		return count(self::arrayTrimFilterUnescape(self::explodeList($sep, $list)));
	}

	/**
	 * This function directs the delimiter replacement operation for the lstsep function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function lstsepRender($parser, $frame, $params) {
		$inList = isset($params[0]) ? trim($frame->expand($params[0])) : '';
		if ($inList === '') {
			return '';
		}

		$inSep = isset($params[1]) ? ParserPower::unescape(trim($frame->expand($params[1]))) : ',';

		$inSep = $parser->mStripState->unstripNoWiki($inSep);

		$values = self::arrayTrimFilterUnescape(self::explodeList($inSep, $inList));
		if (count($values) === 0) {
			return '';
		}

		$outSep = isset($params[2]) ? ParserPower::unescape(trim($frame->expand($params[2]))) : ', ';

		return [implode($outSep, $values), 'noparse' => false];
	}

	/**
	 * This function directs the list element retrieval operation for the lstelem function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function lstelemRender($parser, $frame, $params) {
		$inList = isset($params[0]) ? trim($frame->expand($params[0])) : '';
		if ($inList === '') {
			return '';
		}

		$inSep = isset($params[1]) ? ParserPower::unescape(trim($frame->expand($params[1]))) : ',';
		$inIndex = isset($params[2]) ? ParserPower::unescape(trim($frame->expand($params[2]))) : '';

		$inSep = $parser->mStripState->unstripNoWiki($inSep);

		$index = is_numeric($inIndex) ? intval($inIndex) : 1;

		return [self::arrayElementTrimUnescape($index, self::explodeList($inSep, $inList)), 'noparse' => false];
	}

	/**
	 * This function directs the list subdivision and delimiter replacement operation for the lstsub function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function lstsubRender($parser, $frame, $params) {
		$inList = isset($params[0]) ? trim($frame->expand($params[0])) : '';
		if ($inList === '') {
			return '';
		}

		$inSep = isset($params[1]) ? ParserPower::unescape(trim($frame->expand($params[1]))) : ',';
		$inOffset = isset($params[3]) ? ParserPower::unescape(trim($frame->expand($params[3]))) : '';
		$inLength = isset($params[4]) ? ParserPower::unescape(trim($frame->expand($params[4]))) : '';

		$inSep = $parser->mStripState->unstripNoWiki($inSep);

		$offset = is_numeric($inOffset) ? intval($inOffset) : 0;
		$length = is_numeric($inLength) ? intval($inLength) : null;

		$values = self::arrayTrimSliceUnescape($offset, $length, self::explodeList($inSep, $inList));
		if (count($values) === 0) {
			return '';
		}

		$outSep = isset($params[2]) ? ParserPower::unescape(trim($frame->expand($params[2]))) : ', ';

		return [implode($outSep, $values), 'noparse' => false];
	}

	/**
	 * This function directs the search operation for the lstfnd function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function lstfndRender($parser, $frame, $params) {
		$list = isset($params[1]) ? trim($frame->expand($params[1])) : '';
		if ($list === '') {
			return '';
		}

		$item = isset($params[0]) ? ParserPower::unescape(trim($frame->expand($params[0]))) : '';
		$sep = isset($params[2]) ? ParserPower::unescape(trim($frame->expand($params[2]))) : ',';
		$csOption = isset($params[3]) ? strtolower(trim($frame->expand($params[3]))) : 'ncs';

		$sep = $parser->mStripState->unstripNoWiki($sep);

		$values = self::arrayTrimFilterUnescape(self::explodeList($sep, $list));
		if ($csOption === 'cs') {
			foreach ($values as $value) {
				if ($value === $item) {
					return [$value, 'noparse' => false];
				}
			}
		} else {
			foreach ($values as $value) {
				if (strtolower($value) === strtolower($item)) {
					return [$value, 'noparse' => false];
				}
			}
		}
		return '';
	}

	/**
	 * This function directs the search operation for the lstind function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function lstindRender($parser, $frame, $params) {
		$list = isset($params[1]) ? trim($frame->expand($params[1])) : '';
		if ($list === '') {
			return '';
		}

		$item = isset($params[0]) ? ParserPower::unescape(trim($frame->expand($params[0]))) : '';
		$sep = isset($params[2]) ? ParserPower::unescape(trim($frame->expand($params[2]))) : ',';
		$inOptions = isset($params[3]) ? strtolower(trim($frame->expand($params[3]))) : '';

		$sep = $parser->mStripState->unstripNoWiki($sep);
		$options = self::indexOptionsFromParam($inOptions);

		$values = self::arrayTrimFilterUnescape(self::explodeList($sep, $list));
		$count = (is_array($values) || $values instanceof Countable) ? count($values) : 0;
		if ($options & self::INDEX_DESC) {
			if ($options & self::INDEX_CS) {
				for ($index = $count - 1; $index > -1; --$index) {
					if ($values[$index] === $item) {
						return ($options & self::INDEX_NEG) ? $index - $count : $index + 1;
					}
				}
			} else {
				for ($index = $count - 1; $index > -1; --$index) {
					if (strtolower($values[$index]) === strtolower($item)) {
						return ($options & self::INDEX_NEG) ? $index - $count : $index + 1;
					}
				}
			}
		} else {
			if ($options & self::INDEX_CS) {
				for ($index = 0; $index < $count; ++$index) {
					if ($values[$index] === $item) {
						return ($options & self::INDEX_NEG) ? $index - $count : $index + 1;
					}
				}
			} else {
				for ($index = 0; $index < $count; ++$index) {
					if (strtolower($values[$index]) === strtolower($item)) {
						return ($options & self::INDEX_NEG) ? $index - $count : $index + 1;
					}
				}
			}
		}
		return '';
	}

	/**
	 * This function directs the append operation for the lstapp function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return array The function output along with relevant parser options.
	 */
	public static function lstappRender($parser, $frame, $params) {
		$value = isset($params[2]) ? ParserPower::unescape(trim($frame->expand($params[2]))) : '';
		$list = isset($params[0]) ? trim($frame->expand($params[0])) : '';
		if ($list === '') {
			return [$value, 'noparse' => false];
		}

		$sep = isset($params[1]) ? ParserPower::unescape(trim($frame->expand($params[1]))) : ',';

		$sep = $parser->mStripState->unstripNoWiki($sep);

		$values = self::arrayTrimFilterUnescape(self::explodeList($sep, $list));
		if ($value !== '') {
			$values[] = $value;
		}

		return [implode($sep, $values), 'noparse' => false];

	}

	/**
	 * This function directs the prepend operation for the lstprep function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return array The function output along with relevant parser options.
	 */
	public static function lstprepRender($parser, $frame, $params) {
		$value = isset($params[0]) ? ParserPower::unescape(trim($frame->expand($params[0]))) : '';
		$list = isset($params[2]) ? trim($frame->expand($params[2])) : '';
		if ($list === '') {
			return [$value, 'noparse' => false];
		}

		$sep = isset($params[1]) ? ParserPower::unescape(trim($frame->expand($params[1]))) : '';

		$sep = $parser->mStripState->unstripNoWiki($sep);

		$values = self::arrayTrimFilterUnescape(self::explodeList($sep, $list));
		if ($value !== '') {
			array_unshift($values, $value);
		}

		return [implode($sep, $values), 'noparse' => false];
	}

	/**
	 * This function directs the joining operation for the lstjoin function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function lstjoinRender($parser, $frame, $params) {
		$inList1 = isset($params[0]) ? trim($frame->expand($params[0])) : '';
		$inList2 = isset($params[2]) ? trim($frame->expand($params[2])) : '';
		if ($inList1 === '' && $inList2 === '') {
			return '';
		}

		if ($inList1 !== '') {
			$inSep1 = isset($params[1]) ? ParserPower::unescape(trim($frame->expand($params[1]))) : '';

			$inSep1 = $parser->mStripState->unstripNoWiki($inSep1);

			$values1 = self::arrayTrimFilterUnescape(self::explodeList($inSep1, $inList1));
		} else {
			$values1 = [];
		}

		if ($inList2 !== '') {
			$inSep2 = isset($params[3]) ? ParserPower::unescape(trim($frame->expand($params[3]))) : '';

			$inSep2 = $parser->mStripState->unstripNoWiki($inSep2);

			$values2 = self::arrayTrimFilterUnescape(self::explodeList($inSep2, $inList2));
		} else {
			$values2 = [];
		}
		
		$outValues = array_merge($values1, $values2);
		if (count($outValues) === 0) {
			return '';
		}

		$outSep = isset($params[4]) ? ParserPower::unescape(trim($frame->expand($params[4]))) : ', ';

		return [implode($outSep, $outValues), 'noparse' => false];
	}

	/**
	 * Replaces the indicated token in the pattern with the input value.
	 *
	 * @param Parser  $parser  The parser object.
	 * @param PPFrame $frame   The parser frame object.
	 * @param string  $inValue The value to change into one or more template parameters.
	 * @param string  $token   The token to replace.
	 * @param string  $pattern Pattern containing token to be replaced with the input value.
	 *
	 * @return string The result of the token replacement within the pattern.
	 */
	private static function applyPattern($parser, $frame, $inValue, $token, $pattern) {
		return ParserPower::applyPattern($parser, $frame, $inValue, $token, $pattern);
	}

	/**
	 * Replaces the indicated index token in the pattern with the given index and the token
	 * in the pattern with the input value.
	 *
	 * @param Parser  $parser     The parser object.
	 * @param PPFrame $frame      The parser frame object.
	 * @param string  $inValue    The value to change into one or more template parameters.
	 * @param int     $indexToken The token to replace with the index, or null/empty value to skip index replacement.
	 * @param int     $index      The numeric index of this value.
	 * @param string  $token      The token to replace.
	 * @param string  $pattern    Pattern containing token to be replaced with the input value.
	 *
	 * @return string The result of the token replacement within the pattern.
	 */
	private static function applyPatternWithIndex($parser, $frame, $inValue, $indexToken, $index, $token, $pattern) {
		return ParserPower::applyPatternWithIndex($parser, $frame, $inValue, $indexToken, $index, $token, $pattern);
	}

	/**
	 * Breaks the input value into fields and then replaces the indicated tokens in the pattern with those field values.
	 *
	 * @param Parser  $parser     The parser object.
	 * @param PPFrame $frame      The parser frame object.
	 * @param string  $inValue    The value to change into one or more template parameters
	 * @param string  $fieldSep   The delimiter separating the fields in the value.
	 * @param array   $tokens     The list of tokens to replace.
	 * @param int     $tokenCount The number of tokens.
	 * @param string  $pattern    Pattern containing tokens to be replaced by field values.
	 *
	 * @return string The result of the token replacement within the pattern.
	 */
	private static function applyFieldPattern($parser, $frame, $inValue, $fieldSep, $tokens, $tokenCount, $pattern) {
		return ParserPower::applyFieldPattern($parser, $frame, $inValue, $fieldSep, $tokens, $tokenCount, $pattern);
	}

	/**
	 * Replaces the index token with the given index, and then breaks the input value into fields and then replaces the
	 * indicated tokens in the pattern with those field values.
	 *
	 * @param Parser  $parser     The parser object.
	 * @param PPFrame $frame      The parser frame object.
	 * @param string  $inValue    The value to change into one or more template parameters
	 * @param string  $fieldSep   The delimiter separating the fields in the value.
	 * @param int     $indexToken The token to replace with the index, or null/empty value to skip index replacement.
	 * @param int     $index      The numeric index of this value.
	 * @param array   $tokens     The list of tokens to replace.
	 * @param int     $tokenCount The number of tokens.
	 * @param string  $pattern    Pattern containing tokens to be replaced by field values.
	 *
	 * @return string The result of the token replacement within the pattern.
	 */
	private static function applyFieldPatternWithIndex(
		$parser,
		$frame,
		$inValue,
		$fieldSep,
		$indexToken,
		$index,
		$tokens,
		$tokenCount,
		$pattern
	) {
		return ParserPower::applyFieldPatternWithIndex(
			$parser,
			$frame,
			$inValue,
			$fieldSep,
			$indexToken,
			$index,
			$tokens,
			$tokenCount,
			$pattern
		);
	}

	/**
	 * Wraps the given intro and outro around the given content after replacing a given count token
	 * in the intro or outro with the given count.
	 *
	 * @param string $intro      The intro text.
	 * @param string $content    The inner content.
	 * @param string $outro      The outro test.
	 * @param string $countToken The token to replace with count. Null or empty to skip.
	 * @param int    $count      The count to replace the token with.
	 *
	 * @return string The content wrapped by the intro and outro.
	 */
	private static function applyIntroAndOutro($intro, $content, $outro, $countToken, $count) {
		if ($countToken !== null && $countToken !== '') {
			$count = strval($count);
			$intro = str_replace($countToken, $count, $intro);
			$outro = str_replace($countToken, $count, $outro);
		}
		return $intro . $content . $outro;
	}

	/**
	 * Turns the input value into one or more template parameters, processes the templates with those parameters, and
	 * returns the result.
	 *
	 * @param Parser  $parser   The parser object.
	 * @param PPFrame $frame    The parser frame object.
	 * @param string  $inValue  The value to change into one or more template parameters.
	 * @param string  $template The template to pass the parameters to.
	 * @param string  $fieldSep The delimiter separating the parameter values.
	 *
	 * @return string The result of the template.
	 */
	private static function applyTemplate($parser, $frame, $inValue, $template, $fieldSep) {
		$inValue = trim($inValue);
		if ($inValue === '') {
			return;
		}

		if ($fieldSep === '') {
			$outValue = $frame->virtualBracketedImplode('{{', '|', '}}', $template, '1=' . $inValue);
		} else {
			$inFields = explode($fieldSep, $inValue);
			$outFields = [$template];
			$index = 1;
			foreach ($inFields as $inField) {
				$outFields[] = $index . '=' . $inField;
				++$index;
			}
			$outValue = $frame->virtualBracketedImplode('{{', '|', '}}', $outFields);
		}
		if ($outValue instanceof PPNode_Hash_Array) {
			$outValue = $outValue->value;
		}

		return $parser->replaceVariables(implode('', $outValue), $frame);
	}

	/**
	 * This function performs the filtering operation for the listfiler function when done by value inclusion.
	 *
	 * @param array  $inValues Array with the input values.
	 * @param string $values   The list of values to include, not yet exploded.
	 * @param string $valueSep The delimiter separating the values to include.
	 * @param bool   $valueCS  true to match in a case-sensitive manner, false to match in a case-insensitive manner
	 *
	 * @return array The function output.
	 */
	private static function filterListByInclusion($inValues, $values, $valueSep, $valueCS) {
		if ($valueSep !== '') {
			$includeValues = self::arrayTrimFilterUnescape(self::explodeList($valueSep, $values));
		} else {
			$includeValues = [ParserPower::unescape(trim($values))];
		}

		$outValues = [];

		if ($valueCS) {
			foreach ($inValues as $inValue) {
				if (in_array($inValue, $includeValues) === true) {
					$outValues[] = $inValue;
				}
			}
		} else {
			$includeValues = array_map('strtolower', $includeValues);
			foreach ($inValues as $inValue) {
				if (in_array(strtolower($inValue), $includeValues) === true) {
					$outValues[] = $inValue;
				}
			}
		}

		return $outValues;
	}

	/**
	 * This function performs the filtering operation for the listfiler function when done by value exclusion.
	 *
	 * @param array  $inValues Array with the input values.
	 * @param string $values   The list of values to exclude, not yet exploded.
	 * @param string $valueSep The delimiter separating the values to exclude.
	 * @param bool   $valueCS  true to match in a case-sensitive manner, false to match in a case-insensitive manner
	 *
	 * @return array The function output.
	 */
	private static function filterListByExclusion($inValues, $values, $valueSep, $valueCS) {
		if ($valueSep !== '') {
			$excludeValues = self::arrayTrimFilterUnescape(self::explodeList($valueSep, $values));
		} else {
			$excludeValues = [ParserPower::unescape(trim($values))];
		}

		$outValues = [];

		if ($valueCS) {
			foreach ($inValues as $inValue) {
				if (in_array($inValue, $excludeValues) === false) {
					$outValues[] = $inValue;
				}
			}
		} else {
			$excludeValues = array_map('strtolower', $excludeValues);
			foreach ($inValues as $inValue) {
				if (in_array(strtolower($inValue), $excludeValues) === false) {
					$outValues[] = $inValue;
				}
			}
		}

		return $outValues;
	}

	/**
	 * This function performs the filtering operation for the listfilter function when done by pattern.
	 *
	 * @param Parser  $parser     The parser object.
	 * @param PPFrame $frame      The parser frame object.
	 * @param array   $inValues   Array with the input values.
	 * @param string  $fieldSep   Separator between fields, if any.
	 * @param string  $indexToken Replace the current 1-based index of the element. Null/empty to skip.
	 * @param string  $token      The token(s) in the pattern that represents where the list value should go.
	 * @param string  $tokenSep   The separator between tokens if used.
	 * @param string  $pattern    The pattern of text containing token that list values are inserted into at that token.
	 *
	 * @return array The array stripped of any values with non-unique keys.
	 */
	private static function filterFromListByPattern(
		$parser,
		$frame,
		$inValues,
		$fieldSep,
		$indexToken,
		$token,
		$tokenSep,
		$pattern
	) {
		$outValues = [];
		$index = 1;

		if ($fieldSep !== '' && $tokenSep !== '') {
			$tokens = self::arrayTrimUnescape(explode($tokenSep, $token));
			$tokenCount = count($tokens);
			foreach ($inValues as $value) {
				if (trim($value) !== '') {
					$result = self::applyFieldPatternWithIndex(
						$parser,
						$frame,
						$value,
						$fieldSep,
						$indexToken,
						$index,
						$tokens,
						$tokenCount,
						$pattern
					);
					$result = strtolower($parser->replaceVariables($result, $frame));
					if ($result !== 'remove') {
						$outValues[] = $value;
					}
					++$index;
				}
			}
		} else {
			foreach ($inValues as $value) {
				if (trim($value) !== '') {
					$result = self::applyPatternWithIndex(
						$parser,
						$frame,
						$value,
						$indexToken,
						$index,
						$token,
						$pattern
					);
					$result = strtolower($parser->replaceVariables($result, $frame));
					if ($result !== 'remove') {
						$outValues[] = $value;
					}
					++$index;
				}
			}
		}

		return $outValues;
	}

	/**
	 * This function performs the filtering operation for the listfilter function when done by template.
	 *
	 * @param Parser  $parser   The parser object.
	 * @param PPFrame $frame    The parser frame object.
	 * @param array   $inValues Array with the input values.
	 * @param string  $template The template to use.
	 * @param string  $fieldSep Separator between fields, if any.
	 *
	 * @return array The array stripped of any values with non-unique keys.
	 */
	private static function filterFromListByTemplate($parser, $frame, $inValues, $template, $fieldSep) {
		$outValues = [];

		foreach ($inValues as $value) {
			if ($value !== '') {
				$result = self::applyTemplate($parser, $frame, $value, $template, $fieldSep);
				if (strtolower($result) !== 'remove') {
					$outValues[] = $value;
				}
			}
		}

		return $outValues;
	}

	/**
	 * This function renders the listfilter function, sending it to the appropriate processing function based on what
	 * parameter values are provided.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function listfilterRender($parser, $frame, $params) {
		$params = ParserPower::arrangeParams($frame, $params);

		$inList = isset($params["list"]) ? trim($frame->expand($params["list"])) : '';
		if ($inList === '') {
			return isset($params["default"]) ? [ ParserPower::unescape(trim($frame->expand($params["default"]))),
				'noparse' => false
			] : '';
		}

		$inSep = isset($params["insep"]) ? ParserPower::unescape(trim($frame->expand($params["insep"]))) : ',';

		$inSep = $parser->mStripState->unstripNoWiki($inSep);

		$inValues = self::arrayTrimFilterUnescape(self::explodeList($inSep, $inList));
		if (count($inValues) === 0) {
			return isset($params["default"]) ? [ ParserPower::unescape(trim($frame->expand($params["default"]))),
				'noparse' => false
			] : '';
		}

		$keepValues = isset($params["keep"]) ? trim($frame->expand($params["keep"])) : '';

		if ($keepValues !== '') {
			$keepSep = isset($params["keepsep"]) ? trim($frame->expand($params["keepsep"])) : ',';
			$keepCS = isset($params["keepcs"]) ? strtolower(trim($frame->expand($params["keepcs"]))) : 'no';

			$outValues = self::filterListByInclusion($inValues, $keepValues, $keepSep, ($keepCS === 'yes'));
		} else {
			$removeValues = isset($params["remove"]) ? trim($frame->expand($params["remove"])) : '';

			if ($removeValues !== '') {
				$removeSep = isset($params["removesep"]) ? trim($frame->expand($params["removesep"])) : ',';
				$removeCS = isset($params["removecs"]) ? strtolower(trim($frame->expand($params["removecs"]))) : 'no';

				$outValues = self::filterListByExclusion($inValues, $removeValues, $removeSep, ($removeCS === 'yes'));
			} else {
				$template = isset($params["template"]) ? trim($frame->expand($params["template"])) : '';
				$fieldSep = isset($params["fieldsep"]) ?
					ParserPower::unescape(trim($frame->expand($params["fieldsep"]))) : '';

				if ($template !== '') {
					$outValues = self::filterFromListByTemplate($parser, $frame, $inValues, $template, $fieldSep);
				} else {
					$indexToken = isset($params["indextoken"]) ?
						ParserPower::unescape(trim($frame->expand($params["indextoken"]))) : '';
					$token = isset($params["token"]) ? $frame->expand($params["token"]) : '';
					$tokenSep = isset($params["tokensep"]) ?
						ParserPower::unescape(trim($frame->expand($params["tokensep"]))) : ',';
					$pattern = isset($params["pattern"]) ? $params["pattern"] : '';

					$tokenSep = $parser->mStripState->unstripNoWiki($tokenSep);

					$outValues = self::filterFromListByPattern(
						$parser,
						$frame,
						$inValues,
						$fieldSep,
						$indexToken,
						$token,
						$tokenSep,
						$pattern
					);
				}
			}
		}
		if (count($outValues) === 0) {
			return isset($params["default"]) ? [ ParserPower::unescape(trim($frame->expand($params["default"]))),
				'noparse' => false
			] : '';
		}

		$outSep = isset($params["outsep"]) ? ParserPower::unescape(trim($frame->expand($params["outsep"]))) : ', ';
		$countToken = isset($params["counttoken"]) ?
			ParserPower::unescape(trim($frame->expand($params["counttoken"]))) : '';
		$intro = isset($params["intro"]) ? ParserPower::unescape(trim($frame->expand($params["intro"]))) : '';
		$outro = isset($params["outro"]) ? ParserPower::unescape(trim($frame->expand($params["outro"]))) : '';

		$outList = implode($outSep, $outValues);
		$count = count($outValues);
		return [self::applyIntroAndOutro($intro, $outList, $outro, $countToken, $count), 'noparse' => false];
	}

	/**
	 * This function renders the lstfltr function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function lstfltrRender($parser, $frame, $params) {
		$inList = isset($params[2]) ? trim($frame->expand($params[2])) : '';
		if ($inList === '') {
			return '';
		}

		$inSep = isset($params[3]) ? ParserPower::unescape(trim($frame->expand($params[3]))) : ',';

		$inSep = $parser->mStripState->unstripNoWiki($inSep);

		$inValues = self::arrayTrimFilterUnescape(self::explodeList($inSep, $inList));
		if (count($inValues) === 0) {
			return '';
		}

		$values = isset($params[0]) ? trim($frame->expand($params[0])) : '';
		$valueSep = isset($params[1]) ? ParserPower::unescape(trim($frame->expand($params[1]))) : ',';
		$csOption = isset($params[5]) ? strtolower(trim($frame->expand($params[5]))) : 'ncs';

		$outValues = self::filterListByInclusion(
			$inValues,
			$values,
			$valueSep,
			($csOption === 'cs')
		);
		if (count($outValues) === 0) {
			return '';
		}

		$outSep = isset($params[4]) ? ParserPower::unescape(trim($frame->expand($params[4]))) : ', ';

		return [implode($outSep, $outValues), 'noparse' => false];
	}

	/**
	 * This function renders the lstrm function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function lstrmRender($parser, $frame, $params) {
		$inList = isset($params[1]) ? trim($frame->expand($params[1])) : '';
		if ($inList === '') {
			return '';
		}

		$inSep = isset($params[2]) ? ParserPower::unescape(trim($frame->expand($params[2]))) : ',';

		$inSep = $parser->mStripState->unstripNoWiki($inSep);

		$inValues = self::arrayTrimFilterUnescape(self::explodeList($inSep, $inList));
		if (count($inValues) === 0) {
			return '';
		}

		$value = isset($params[0]) ? trim($frame->expand($params[0])) : '';
		$csOption = isset($params[4]) ? strtolower(trim($frame->expand($params[4]))) : 'ncs';

		$outValues = self::filterListByExclusion($inValues, $value, '', ($csOption === 'cs'));
		if (count($outValues) === 0) {
			return '';
		}

		$outSep = isset($params[3]) ? ParserPower::unescape(trim($frame->expand($params[3]))) : ', ';

		return [implode($outSep, $outValues), 'noparse' => false];
	}

	/**
	 * This function reduces an array to unique values.
	 *
	 * @param array $values  The array of values to reduce to unique values.
	 * @param bool  $valueCS True to determine uniqueness case-sensitively, false to determine it case-insensitively.
	 *
	 * @return array The function output along with relevant parser options.
	 */
	public static function reduceToUniqueValues($values, $valueCS) {
		if ($valueCS) {
			return array_unique($values);
		} else {
			return array_intersect_key($values, array_unique(array_map('strtolower', $values)));
		}
	}

	/**
	 * This function directs the counting operation for the lstcntuniq function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string The function output.
	 */
	public static function lstcntuniqRender($parser, $frame, $params) {
		$list = isset($params[0]) ? trim($frame->expand($params[0])) : '';
		if ($list === '') {
			return '0';
		}

		$sep = isset($params[1]) ? ParserPower::unescape(trim($frame->expand($params[1]))) : ',';

		$sep = $parser->mStripState->unstripNoWiki($sep);

		$values = self::arrayTrimFilterUnescape(self::explodeList($sep, $list));
		if (count($values) === 0) {
			return '0';
		}

		$csOption = isset($params[2]) ? strtolower(trim($frame->expand($params[2]))) : 'ncs';

		$values = self::reduceToUniqueValues($values, $csOption === 'cs');
		return count($values);
	}

	/**
	 * Generates keys by replacing tokens in a pattern with the fields in the values, excludes any value that generates
	 * any key generated by the previous values, and returns an array of the nonexcluded values.
	 *
	 * @param Parser  $parser     The parser object.
	 * @param PPFrame $frame      The parser frame object.
	 * @param array   $inValues   Array with the input values.
	 * @param string  $fieldSep   Separator between fields, if any.
	 * @param string  $indexToken Replace the current 1-based index of the element. Null/empty to skip.
	 * @param string  $token      The token in the pattern that represents where the list value should go.
	 * @param array   $tokens     Or if there are mulitple fields, the tokens representing where they go.
	 * @param string  $pattern    The pattern of text containing token that list values are inserted into at that token.
	 *
	 * @return array An array with only values that generated unique keys via the given pattern.
	 */
	private static function reduceToUniqueValuesByKeyPattern(
		$parser,
		$frame,
		$inValues,
		$fieldSep,
		$indexToken,
		$token,
		$tokens,
		$pattern
	) {
		$previousKeys = [];
		$outValues = [];
		$index = 1;
		if ((isset($tokens) && is_array($tokens))) {
			$tokenCount = count($tokens);
			foreach ($inValues as $value) {
				if (trim($value) !== '') {
					$key = self::applyFieldPatternWithIndex(
						$parser,
						$frame,
						$value,
						$fieldSep,
						$indexToken,
						$index,
						$tokens,
						$tokenCount,
						$pattern
					);
					$key = $parser->replaceVariables($key, $frame);
					if (!in_array($key, $previousKeys)) {
						$previousKeys[] = $key;
						$outValues[] = $value;
					}
					++$index;
				}
			}
		} else {
			foreach ($inValues as $value) {
				if (trim($value) !== '') {
					$key = self::applyPatternWithIndex($parser, $frame, $value, $indexToken, $index, $token, $pattern);
					$key = $parser->replaceVariables($key, $frame);
					if (!in_array($key, $previousKeys)) {
						$previousKeys[] = $key;
						$outValues[] = $value;
					}
					++$index;
				}
			}
		}

		return $outValues;
	}

	/**
	 * Generates keys by turning the input value into one or more template parameters and processing that template,
	 * excludes any value that generates any key generated by the previous values, and returns an array of the
	 * nonexcluded values.
	 *
	 * @param Parser  $parser   The parser object.
	 * @param PPFrame $frame    The parser frame object.
	 * @param array   $inValues Array with the input values.
	 * @param string  $template The template to use.
	 * @param string  $fieldSep Separator between fields, if any.
	 *
	 * @return array An array with only values that generated unique keys via the given pattern.
	 */
	private static function reduceToUniqueValuesByKeyTemplate($parser, $frame, $inValues, $template, $fieldSep) {
		$previousKeys = [];
		$outValues = [];

		foreach ($inValues as $value) {
			$key = self::applyTemplate($parser, $frame, $value, $template, $fieldSep);
			if (!in_array($key, $previousKeys)) {
				$previousKeys[] = $key;
				$outValues[] = $value;
			}
		}

		return $outValues;
	}

	/**
	 * This function renders the listunique function, sending it to the appropriate processing function based on what
	 * parameter values are provided.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function listuniqueRender($parser, $frame, $params) {
		$params = ParserPower::arrangeParams($frame, $params);

		$inList = isset($params["list"]) ? trim($frame->expand($params["list"])) : '';
		if ($inList === '') {
			return isset($params["default"]) ? [ ParserPower::unescape(trim($frame->expand($params["default"]))),
				'noparse' => false
			] : '';
		}

		$inSep = isset($params["insep"]) ? ParserPower::unescape(trim($frame->expand($params["insep"]))) : ',';

		$inSep = $parser->mStripState->unstripNoWiki($inSep);

		$inValues = self::arrayTrimFilterUnescape(self::explodeList($inSep, $inList));
		if ($inList === '') {
			return isset($params["default"]) ? [ ParserPower::unescape(trim($frame->expand($params["default"]))),
				'noparse' => false
			] : '';
		}

		$template = isset($params["template"]) ? trim($frame->expand($params["template"])) : '';
		$fieldSep = isset($params["fieldsep"]) ? ParserPower::unescape(trim($frame->expand($params["fieldsep"]))) : '';

		if ($template !== '') {
			$outValues = self::reduceToUniqueValuesByKeyTemplate($parser, $frame, $inValues, $template, $fieldSep);
		} else {
			$indexToken = isset($params["indextoken"]) ?
				ParserPower::unescape(trim($frame->expand($params["indextoken"]))) : '';
			$token = isset($params["token"]) ? ParserPower::unescape(trim($frame->expand($params["token"]))) : '';
			$pattern = isset($params["pattern"]) ? $params["pattern"] : '';

			if (($indexToken !== '' || $token !== '') && $pattern !== '') {
				$tokenSep = isset($params["tokensep"]) ?
					ParserPower::unescape(trim($frame->expand($params["tokensep"]))) : ',';

				if ($fieldSep !== '' && $tokenSep !== '') {
					$tokens = self::arrayTrimUnescape(explode($tokenSep, $token));
				}

				$outValues = self::reduceToUniqueValuesByKeyPattern(
					$parser,
					$frame,
					$inValues,
					$fieldSep,
					$indexToken,
					$token,
					isset($tokens) ? $tokens : null,
					$pattern
				);
			} else {
				$uniqueCS = isset($params["uniquecs"]) ? strtolower(trim($frame->expand($params["uniquecs"]))) : 'no';

				$outValues = self::reduceToUniqueValues($inValues, $uniqueCS === 'yes');
			}
		}

		$outSep = isset($params["outsep"]) ? ParserPower::unescape(trim($frame->expand($params["outsep"]))) : ', ';
		$countToken = isset($params["counttoken"]) ?
			ParserPower::unescape(trim($frame->expand($params["counttoken"]))) : '';
		$intro = isset($params["intro"]) ? ParserPower::unescape(trim($frame->expand($params["intro"]))) : '';
		$outro = isset($params["outro"]) ? ParserPower::unescape(trim($frame->expand($params["outro"]))) : '';

		$outList = implode($outSep, $outValues);
		$count = count($outValues);
		return [self::applyIntroAndOutro($intro, $outList, $outro, $countToken, $count), 'noparse' => false];
	}

	/**
	 * This function converts a string containing sort option keywords into an integer of sort option flags.
	 *
	 * @param string $param   The string containg sort options keywords.
	 * @param int    $default Any flags that should be set by default.
	 *
	 * @return int The flags representing the requested options.
	 */
	private static function sortOptionsFromParam($param, $default = 0) {
		$optionKeywords = explode(' ', $param);
		$options = $default;

		foreach ($optionKeywords as $optionKeyword) {
			switch (strtolower(trim($optionKeyword))) {
				case 'numeric':
					$options |= self::SORT_NUMERIC;
					break;
				case 'alpha':
					$options &= ~self::SORT_NUMERIC;
					break;
				case 'cs':
					$options |= self::SORT_CS;
					break;
				case 'ncs':
					$options &= ~self::SORT_CS;
					break;
				case 'desc':
					$options |= self::SORT_DESC;
					break;
				case 'asc':
					$options &= ~self::SORT_DESC;
					break;
			}
		}

		return $options;
	}

	/**
	 * This function directs the duplicate removal function for the lstuniq function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function lstuniqRender($parser, $frame, $params) {
		$inList = isset($params[0]) ? trim($frame->expand($params[0])) : '';
		if ($inList === '') {
			return '';
		}

		$inSep = isset($params[1]) ? ParserPower::unescape(trim($frame->expand($params[1]))) : ',';

		$inSep = $parser->mStripState->unstripNoWiki($inSep);

		$values = self::arrayTrimFilterUnescape(self::explodeList($inSep, $inList));
		if (count($values) === 0) {
			return '';
		}

		$outSep = isset($params[2]) ? ParserPower::unescape(trim($frame->expand($params[2]))) : ', ';
		$csOption = isset($params[3]) ? strtolower(trim($frame->expand($params[3]))) : 'ncs';

		$values = self::reduceToUniqueValues($values, $csOption === 'cs');
		return [implode($outSep, $values), 'noparse' => false];
	}

	/**
	 * This function converts a string containing index option keywords into an integer of index option flags.
	 *
	 * @param string $param   The string containg index options keywords.
	 * @param int    $default Any flags that should be set by default.
	 *
	 * @return int The flags representing the requested options.
	 */
	private static function indexOptionsFromParam($param, $default = 0) {
		$optionKeywords = explode(' ', $param);
		$options = $default;

		foreach ($optionKeywords as $optionKeyword) {
			switch (strtolower(trim($optionKeyword))) {
				case 'neg':
					$options |= self::INDEX_NEG;
					break;
				case 'pos':
					$options &= ~self::INDEX_NEG;
					break;
				case 'cs':
					$options |= self::INDEX_CS;
					break;
				case 'ncs':
					$options &= ~self::INDEX_CS;
					break;
				case 'desc':
					$options |= self::INDEX_DESC;
					break;
				case 'asc':
					$options &= ~self::INDEX_DESC;
					break;
			}
		}

		return $options;
	}

	/**
	 * This function sorts an array according to the parameters supplied.
	 *
	 * @param array  $values      An array of values to sort.
	 * @param string $optionParam The sorting options parameter value as provided by the user.
	 *
	 * @return array The values in an array of strings.
	 */
	private static function sortList($values, $optionParam) {
		$options = self::sortOptionsFromParam($optionParam);

		if ($options & self::SORT_NUMERIC) {
			if ($options & self::SORT_DESC) {
				rsort($values, SORT_NUMERIC);
				return $values;
			} else {
				sort($values, SORT_NUMERIC);
				return $values;
			}
		} else {
			if ($options & self::SORT_CS) {
				if ($options & self::SORT_DESC) {
					rsort($values, SORT_STRING);
					return $values;
				} else {
					sort($values, SORT_STRING);
					return $values;
				}
			} else {
				if ($options & self::SORT_DESC) {
					usort($values, 'ParserPower\\ParserPowerCompare::rstrcasecmp');
					return $values;
				} else {
					usort($values, 'strcasecmp');
					return $values;
				}
			}
		}
	}

	/**
	 * This takes an array where each element is an array with a sort key in element 0 and a value in element 1, and it
	 * returns an array with just the values.
	 *
	 * @param array $pairedValues An array with values paired with sort keys.
	 *
	 * @return array An array with just the values.
	 */
	private static function discardSortKeys($pairedValues) {
		$values = [];

		foreach ($pairedValues as $pairedValue) {
			$values[] = $pairedValue[1];
		}

		return $values;
	}

	/**
	 * This takes an array where each element is an array with a sort key in element 0 and a value in element 1, and it
	 * returns an array with just the sort keys wrapped in <nowiki> tags. Used for debugging purposes.
	 *
	 * @param array $pairedValues An array with values paired with sort keys.
	 *
	 * @return array An array with just the sort keys wrapped in <nowiki>..
	 */
	private static function discardValues($pairedValues) {
		$values = [];

		foreach ($pairedValues as $pairedValue) {
			$values[] = '<nowiki>' . $pairedValue[0] . '</nowiki>';
		}

		return $values;
	}

	/**
	 * This function sorts an array for the listsort function when done by pattern.
	 *
	 * @param Parser  $parser         The parser object.
	 * @param PPFrame $frame          The parser frame object.
	 * @param array   $values         Array with the input values.
	 * @param string  $fieldSep       The delimiter separating values in the input list.
	 * @param string  $indexToken     Replace the current 1-based index of the element. Null/empty to skip.
	 * @param string  $token          The token in the pattern that represents where the list value should go.
	 * @param array   $tokens         Or if there are mulitple fields, the tokens representing where they go.
	 * @param string  $pattern        The pattern containing token that list values are inserted into at that token.
	 * @param string  $sortOptions    A string of options for the key sort as handled by #listsort.
	 * @param string  $subsort        A string indicating whether to perform a value sort where sort keys are equal.
	 * @param string  $subsortOptions A string of options for the value sort as handled by #listsort.
	 *
	 * @return array The array with all values sorted.
	 */
	private static function sortListByPattern(
		$parser,
		$frame,
		$values,
		$fieldSep,
		$indexToken,
		$token,
		$tokens,
		$pattern,
		$sortOptions,
		$subsort,
		$subsortOptions
	) {
		$pairedValues = [];
		$index = 1;

		if ((isset($tokens) && is_array($tokens))) {
			$tokenCount = count($tokens);
			foreach ($values as $value) {
				if (trim($value) !== '') {
					$key = self::applyFieldPatternWithIndex(
						$parser,
						$frame,
						$value,
						$fieldSep,
						$indexToken,
						$index,
						$tokens,
						$tokenCount,
						$pattern
					);
					$key = $parser->replaceVariables($key, $frame);
					$pairedValues[] = [$key, $value];
					++$index;
				}
			}
		} else {
			foreach ($values as $value) {
				if (trim($value) !== '') {
					$key = self::applyPatternWithIndex($parser, $frame, $value, $indexToken, $index, $token, $pattern);
					$key = $parser->replaceVariables($key, $frame);
					$pairedValues[] = [$key, $value];
					++$index;
				}
			}
		}

		$comparer = new ParserPowerSortKeyValueComparer(
			self::sortOptionsFromParam($sortOptions, self::SORT_NUMERIC),
			$subsort === 'yes',
			self::sortOptionsFromParam($subsortOptions)
		);

		usort($pairedValues, [$comparer, 'compare']);

		return self::discardSortKeys($pairedValues);
	}

	/**
	 * This function sorts an array for the listsort function when done by template.
	 *
	 * @param Parser  $parser         The parser object.
	 * @param PPFrame $frame          The parser frame object.
	 * @param array   $values         Array with the input values.
	 * @param string  $template       The template to use.
	 * @param string  $fieldSep       The delimiter separating values in the input list.
	 * @param string  $sortOptions    A string of options for the key sort as handled by #listsort.
	 * @param string  $subsort        A string indicating whether to perform a value sort where sort keys are equal.
	 * @param string  $subsortOptions A string of options for the value sort as handled by #listsort.
	 *
	 * @return array The array with all values sorted.
	 */
	private static function sortListByTemplate(
		$parser,
		$frame,
		$values,
		$template,
		$fieldSep,
		$sortOptions,
		$subsort,
		$subsortOptions
	) {
		$pairedValues = [];

		foreach ($values as $value) {
			$pairedValues[] = [self::applyTemplate($parser, $frame, $value, $template, $fieldSep), $value];
		}

		$comparer = new ParserPowerSortKeyValueComparer(
			self::sortOptionsFromParam($sortOptions, self::SORT_NUMERIC),
			$subsort === 'yes',
			self::sortOptionsFromParam($subsortOptions)
		);

		usort($pairedValues, [$comparer, 'compare']);

		return self::discardSortKeys($pairedValues);
	}

	/**
	 * This function directs the sort operation for the listsort function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function listsortRender($parser, $frame, $params) {
		$params = ParserPower::arrangeParams($frame, $params);

		$inList = isset($params["list"]) ? trim($frame->expand($params["list"])) : '';
		if ($inList === '') {
			return isset($params["default"]) ? [ ParserPower::unescape(trim($frame->expand($params["default"]))),
				'noparse' => false
			] : '';
		}

		$inSep = isset($params["insep"]) ? ParserPower::unescape(trim($frame->expand($params["insep"]))) : ',';

		$inSep = $parser->mStripState->unstripNoWiki($inSep);

		$values = self::arrayTrimFilterUnescape(self::explodeList($inSep, $inList));
		if (count($values) === 0) {
			return isset($params["default"]) ? [ ParserPower::unescape(trim($frame->expand($params["default"]))),
				'noparse' => false
			] : '';
		}

		$template = isset($params["template"]) ? trim($frame->expand($params["template"])) : '';
		$fieldSep = isset($params["fieldsep"]) ? ParserPower::unescape(trim($frame->expand($params["fieldsep"]))) : '';
		$sortOptions = isset($params["sortoptions"]) ? trim($frame->expand($params["sortoptions"])) : '';
		$duplicates = isset($params["duplicates"]) ? strtolower(trim($frame->expand($params["duplicates"]))) : 'keep';
		$subsort = isset($params["subsort"]) ? strtolower(trim($frame->expand($params["subsort"]))) : 'no';
		$subsortOptions = isset($params["subsortoptions"]) ? trim($frame->expand($params["subsortoptions"])) : '';

		if ($duplicates === 'strip') {
			$values = array_unique($values);
		}

		if ($template !== '') {
			$values = self::sortListByTemplate(
				$parser,
				$frame,
				$values,
				$template,
				$fieldSep,
				$sortOptions,
				$subsort,
				$subsortOptions
			);
		} else {
			$indexToken = isset($params["indextoken"]) ?
				ParserPower::unescape(trim($frame->expand($params["indextoken"]))) : '';
			$token = isset($params["token"]) ? ParserPower::unescape(trim($frame->expand($params["token"]))) : '';
			$pattern = isset($params["pattern"]) ? $params["pattern"] : '';

			if (($indexToken !== '' || $token !== '') && $pattern !== '') {
				$tokenSep = isset($params["tokensep"]) ? ParserPower::unescape(trim($frame->expand($params["tokensep"]))) : ',';

				if ($fieldSep !== '' && $tokenSep !== '') {
					$tokens = self::arrayTrimUnescape(explode($tokenSep, $token));
				}

				$values = self::sortListByPattern(
					$parser,
					$frame,
					$values,
					$fieldSep,
					$indexToken,
					$token,
					isset($tokens) ? $tokens : null,
					$pattern,
					$sortOptions,
					$subsort,
					$subsortOptions
				);
			} else {
				$values = self::sortList($values, $sortOptions);
			}
		}

		$outSep = isset($params["outsep"]) ? ParserPower::unescape(trim($frame->expand($params["outsep"]))) : ', ';
		$intro = isset($params["intro"]) ? ParserPower::unescape(trim($frame->expand($params["intro"]))) : '';
		$outro = isset($params["outro"]) ? ParserPower::unescape(trim($frame->expand($params["outro"]))) : '';
		$countToken = isset($params["counttoken"]) ?
			ParserPower::unescape(trim($frame->expand($params["counttoken"]))) : '';

		$outList = implode($outSep, $values);
		$count = count($values);
		return [self::applyIntroAndOutro($intro, $outList, $outro, $countToken, $count), 'noparse' => false];
	}

	/**
	 * This function directs the sort option for the lstsrt function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function lstsrtRender($parser, $frame, $params) {
		$inList = isset($params[0]) ? trim($frame->expand($params[0])) : '';
		if ($inList === '') {
			return '';
		}

		$inSep = isset($params[1]) ? ParserPower::unescape(trim($frame->expand($params[1]))) : ',';

		$inSep = $parser->mStripState->unstripNoWiki($inSep);

		$values = self::arrayTrimFilterUnescape(self::explodeList($inSep, $inList));
		if (count($values) === 0) {
			return '';
		}

		$outSep = isset($params[2]) ? ParserPower::unescape(trim($frame->expand($params[2]))) : ', ';
		$sortOptions = isset($params[3]) ? trim($frame->expand($params[3])) : '';

		$values = self::sortList($values, $sortOptions);
		return [implode($outSep, $values), 'noparse' => false];
	}

	/**
	 * This function performs the pattern changing operation for the listmap function.
	 *
	 * @param Parser  $parser     The parser object.
	 * @param PPFrame $frame      The parser frame object.
	 * @param array   $inValues   Array with the input values.
	 * @param string  $fieldSep   The optional delimiter seoarating fields in each value.
	 * @param string  $indexToken Replace the current 1-based index of the element. Null/empty to skip.
	 * @param string  $token      The token(s) in the pattern that represents where the list value should go.
	 * @param string  $tokenSep   The separator between tokens if used.
	 * @param string  $pattern    The pattern containing token that list values are inserted into at that token.
	 *
	 * @return array The values in an array of strings.
	 */
	private static function applyPatternToList(
		$parser,
		$frame,
		$inValues,
		$fieldSep,
		$indexToken,
		$token,
		$tokenSep,
		$pattern
	) {
		$outValues = [];
		$index = 1;

		if ($fieldSep !== '' && $tokenSep !== '') {
			$tokens = self::arrayTrimUnescape(explode($tokenSep, $token));
			$tokenCount = count($tokens);
			foreach ($inValues as $inValue) {
				if (trim($inValue) !== '') {
					$outValue = self::applyFieldPatternWithIndex(
						$parser,
						$frame,
						$inValue,
						$fieldSep,
						$indexToken,
						$index,
						$tokens,
						$tokenCount,
						$pattern
					);
					if ($outValue !== '') {
						$outValues[] = $outValue;
						++$index;
					}
				}
			}
		} else {
			foreach ($inValues as $inValue) {
				if (trim($inValue) !== '') {
					$outValue = self::applyPatternWithIndex(
						$parser,
						$frame,
						$inValue,
						$indexToken,
						$index,
						$token,
						$pattern
					);
					if ($outValue !== '') {
						$outValues[] = $outValue;
						++$index;
					}
				}
			}
		}

		return $outValues;
	}

	/**
	 * This function performs the sort option for the listmtemp function.
	 *
	 * @param Parser  $parser   The parser object.
	 * @param PPFrame $frame    The parser frame object.
	 * @param array   $inValues Array with the input values.
	 * @param string  $template The template to use.
	 * @param string  $fieldSep The optional delimiter seoarating fields in each value.
	 *
	 * @return array The values in an array of strings.
	 */
	private static function applyTemplateToList($parser, $frame, $inValues, $template, $fieldSep) {
		$outValues = [];

		foreach ($inValues as $inValue) {
			if (trim($inValue) !== '') {
				$outValue = self::applyTemplate($parser, $frame, $inValue, $template, $fieldSep);
				if ($outValue !== '') {
					$outValues[] = $outValue;
				}
			}
		}

		return $outValues;
	}

	/**
	 * This function renders the listmap function, sending it to the appropriate processing function based on what
	 * parameter values are provided.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function listmapRender($parser, $frame, $params) {
		$params = ParserPower::arrangeParams($frame, $params);

		$inList = isset($params["list"]) ? trim($frame->expand($params["list"])) : '';
		if ($inList === '') {
			return isset($params["default"]) ? [ ParserPower::unescape(trim($frame->expand($params["default"]))),
				'noparse' => false
			] : '';
		}

		$inSep = isset($params["insep"]) ? ParserPower::unescape(trim($frame->expand($params["insep"]))) : ',';

		$inSep = $parser->mStripState->unstripNoWiki($inSep);

		$inValues = self::arrayTrimFilterUnescape(self::explodeList($inSep, $inList));
		if (count($inValues) === 0) {
			return isset($params["default"]) ? [ ParserPower::unescape(trim($frame->expand($params["default"]))),
				'noparse' => false
			] : '';
		}

		$template = isset($params["template"]) ? trim($frame->expand($params["template"])) : '';
		$fieldSep = isset($params["fieldsep"]) ? ParserPower::unescape(trim($frame->expand($params["fieldsep"]))) : '';
		$sortMode = isset($params["sortmode"]) ? strtolower(trim($frame->expand($params["sortmode"]))) : 'nosort';
		$sortOptions = isset($params["sortoptions"]) ? trim($frame->expand($params["sortoptions"])) : '';
		$duplicates = isset($params["duplicates"]) ? strtolower(trim($frame->expand($params["duplicates"]))) : 'keep';

		if ($duplicates === 'prestrip' || $duplicates === 'pre/poststrip') {
			$inValues = array_unique($inValues);
		}

		if ($sortMode === 'presort' || $sortMode === 'pre/postsort') {
			$inValues = self::sortList($inValues, $sortOptions);
		}

		if ($template !== '') {
			$outValues = self::applyTemplateToList($parser, $frame, $inValues, $template, $fieldSep);
		} else {
			$indexToken = isset($params["indextoken"]) ?
				ParserPower::unescape(trim($frame->expand($params["indextoken"]))) : '';
			$token = isset($params["token"]) ? ParserPower::unescape(trim($frame->expand($params["token"]))) : '';
			$tokenSep = isset($params["tokensep"]) ?
				ParserPower::unescape(trim($frame->expand($params["tokensep"]))) : ',';
			$pattern = isset($params["pattern"]) ? $params["pattern"] : '';

			$outValues = self::applyPatternToList(
				$parser,
				$frame,
				$inValues,
				$fieldSep,
				$indexToken,
				$token,
				$tokenSep,
				$pattern
			);
		}
		if (count($outValues) === 0) {
			return isset($params["default"]) ? [ ParserPower::unescape(trim($frame->expand($params["default"]))),
				'noparse' => false
			] : '';
		}

		if ($duplicates === 'strip' || $duplicates === 'poststrip' || $duplicates === 'pre/postsort') {
			$outValues = array_unique($outValues);
		}

		if ($sortMode === 'sort' || $sortMode === 'postsort' || $sortMode === 'pre/postsort') {
			$outValues = self::sortList($outValues, $sortOptions);
		}

		$outSep = isset($params["outsep"]) ? ParserPower::unescape(trim($frame->expand($params["outsep"]))) : ', ';
		$countToken = isset($params["counttoken"]) ?
			ParserPower::unescape(trim($frame->expand($params["counttoken"]))) : '';
		$intro = isset($params["intro"]) ? ParserPower::unescape(trim($frame->expand($params["intro"]))) : '';
		$outro = isset($params["outro"]) ? ParserPower::unescape(trim($frame->expand($params["outro"]))) : '';

		$outList = implode($outSep, $outValues);
		$count = count($outValues);
		return [self::applyIntroAndOutro($intro, $outList, $outro, $countToken, $count), 'noparse' => false];
	}

	/**
	 * This function performs the sort option for the listm function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function lstmapRender($parser, $frame, $params) {
		$inList = isset($params[0]) ? trim($frame->expand($params[0])) : '';
		if ($inList === '') {
			return '';
		}

		$inSep = isset($params[1]) ? ParserPower::unescape(trim($frame->expand($params[1]))) : ',';

		$inSep = $parser->mStripState->unstripNoWiki($inSep);

		$values = self::arrayTrimFilterUnescape(self::explodeList($inSep, $inList));
		if (count($values) === 0) {
			return '';
		}

		$token = isset($params[2]) ? ParserPower::unescape(trim($frame->expand($params[2]))) : 'x';
		$pattern = isset($params[3]) ? $params[3] : 'x';
		$sortMode = isset($params[5]) ? strtolower(trim($frame->expand($params[5]))) : 'nosort';
		$sortOptions = isset($params[6]) ? trim($frame->expand($params[6])) : '';

		if ($sortMode === 'presort' || $sortMode === 'pre/postsort') {
			$values = self::sortList($values, $sortOptions);
		}

		$values = self::applyPatternToList(
			$parser,
			$frame,
			$values,
			'',
			'',
			$token,
			'',
			$pattern
		);
		if (count($values) === 0) {
			return '';
		}

		if ($sortMode === 'sort' || $sortMode === 'postsort' || $sortMode === 'pre/postsort') {
			$values = self::sortList($values, $sortOptions);
		}

		$outSep = isset($params[4]) ? ParserPower::unescape(trim($frame->expand($params[4]))) : ', ';

		return [implode($outSep, $values), 'noparse' => false];
	}

	/**
	 * This function performs the sort option for the lstmaptemp function.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function lstmaptempRender($parser, $frame, $params) {
		$inList = isset($params[0]) ? trim($frame->expand($params[0])) : '';
		if ($inList === '') {
			return '';
		}

		$inSep = isset($params[2]) ? ParserPower::unescape(trim($frame->expand($params[2]))) : ',';

		$inSep = $parser->mStripState->unstripNoWiki($inSep);

		$inValues = self::arrayTrimFilterUnescape(self::explodeList($inSep, $inList));
		if (count($inValues) === 0) {
			return '';
		}

		$template = isset($params[1]) ? trim($frame->expand($params[1])) : '';
		$sortMode = isset($params[4]) ? strtolower(trim($frame->expand($params[4]))) : 'nosort';
		$sortOptions = isset($params[5]) ? trim($frame->expand($params[5])) : '';

		if ($sortMode === 'presort' || $sortMode === 'pre/postsort') {
			$inValues = self::sortList($inValues, $sortOptions);
		}

		$outValues = self::applyTemplateToList($parser, $frame, $inValues, $template, '');
		if (count($inValues) === 0) {
			return '';
		}

		if ($sortMode === 'sort' || $sortMode === 'postsort' || $sortMode === 'pre/postsort') {
			$outValues = self::sortList($outValues, $sortOptions);
		}

		$outSep = isset($params[3]) ? ParserPower::unescape(trim($frame->expand($params[3]))) : ', ';

		return [implode($outSep, $outValues), 'noparse' => false];
	}

	/**
	 * Breaks the input values into fields and then replaces the indicated tokens in the pattern
	 * with those field values. This is for special cases when two sets of replacements are
	 * necessary for a given pattern.
	 *
	 * @param Parser  $parser   The parser object.
	 * @param PPFrame $frame    The parser frame object.
	 * @param string  $inValue1 The first value to (potentially) split and replace tokens with
	 * @param string  $inValue2 The second value to (potentially) split and replace tokens with
	 * @param string  $fieldSep The delimiter separating the fields in the value.
	 * @param array   $tokens1  The list of tokens to replace when performing the replacement for $inValue1.
	 * @param array   $tokens2  The list of tokens to replace when performing the replacement for $inValue2.
	 * @param string  $pattern  Pattern containing tokens to be replaced by field (or unsplit) values.
	 *
	 * @return string The result of the token replacement within the pattern.
	 */
	private static function applyTwoSetFieldPattern(
		$parser,
		$frame,
		$inValue1,
		$inValue2,
		$fieldSep,
		$tokens1,
		$tokens2,
		$pattern
	) {
		$inValue1 = trim($inValue1);
		$inValue2 = trim($inValue2);
		$tokenCount1 = count($tokens1);
		$tokenCount2 = count($tokens2);

		if ($inValue1 !== '' && $inValue2 !== '') {
			$outValue = $frame->expand($pattern, PPFrame::NO_ARGS | PPFrame::NO_TEMPLATES);
			if ($inValue1 !== '') {
				$fields = explode($fieldSep, $inValue1, $tokenCount1);
				$fieldCount = count($fields);
				for ($i = 0; $i < $tokenCount1; $i++) {
					$outValue = str_replace($tokens1[$i], ($i < $fieldCount) ? $fields[$i] : '', $outValue);
				}
			}
			if ($inValue2 !== '') {
				$fields = explode($fieldSep, $inValue2, $tokenCount2);
				$fieldCount = count($fields);
				for ($i = 0; $i < $tokenCount2; $i++) {
					$outValue = str_replace($tokens2[$i], ($i < $fieldCount) ? $fields[$i] : '', $outValue);
				}
			}
			$outValue = $parser->preprocessToDom($outValue, $frame->isTemplate() ? Parser::PTD_FOR_INCLUSION : 0);
			return ParserPower::unescape(trim($frame->expand($outValue)));
		}
	}

	/**
	 * Turns the input value into one or more template parameters, processes the templates with those parameters, and
	 * returns the result.
	 *
	 * @param Parser  $parser   The parser object.
	 * @param PPFrame $frame    The parser frame object.
	 * @param string  $inValue1 The first value to change into one or more template parameters.
	 * @param string  $inValue2 The second value to change into one of more template parameters.
	 * @param string  $template The template to pass the parameters to.
	 * @param string  $fieldSep The delimiter separating the parameter values.
	 *
	 * @return string The result of the template.
	 */
	private static function applyTemplateToTwoValues($parser, $frame, $inValue1, $inValue2, $template, $fieldSep) {
		return self::applyTemplate($parser, $frame, $inValue1 . $fieldSep . $inValue2, $template, $fieldSep);
	}

	/**
	 * This function performs repeated merge passes until either the input array is merged to a single value, or until
	 * a merge pass is completed that does not perform any further merges (pre- and post-pass array count is the same).
	 * Each merge pass operates by performing a conditional on all possible pairings of items, immediately merging two
	 * if the conditional indicates it should and reducing the possible pairings. The logic for the conditional and
	 * the actual merge process is supplied through a user-defined function.
	 *
	 * @param Parser  $parser        The parser object.
	 * @param PPFrame $frame         The parser frame object.
	 * @param array   $inValues      Array with the input values, should be already fully preprocessed.
	 * @param string  $applyFunction Valid name of the function to call for both match and merge processes.
	 * @param array   $matchParams   Parameter values for the matching process, with open spots for the values.
	 * @param array   $mergeParams   Parameter values for the merging process, with open spots for the values.
	 * @param int     $valueIndex1   The index in $matchParams and $mergeParams where the first value is to go.
	 * @param int     $valueIndex2   The index in $matchParams and $mergeParams where the second value is to go.
	 *
	 * @return array The function output along with relevant parser options.
	 */
	private static function iterativeListMerge(
		$parser,
		$frame,
		$inValues,
		$applyFunction,
		$matchParams,
		$mergeParams,
		$valueIndex1,
		$valueIndex2
	) {
		$preValues = $inValues;
		$debug1 = $debug2 = $debug3 = 0;

		do {
			$postValues = [];
			$preCount = count($preValues);

			while (count($preValues) > 0) {
				$value1 = $matchParams[$valueIndex1] = $mergeParams[$valueIndex1] = array_shift($preValues);
				$otherValues = $preValues;
				$preValues = [];

				while (count($otherValues) > 0) {
					$value2 = $matchParams[$valueIndex2] = $mergeParams[$valueIndex2] = array_shift($otherValues);
					$doMerge = call_user_func_array($applyFunction, $matchParams);
					$doMerge = strtolower($parser->replaceVariables(ParserPower::unescape(trim($doMerge)), $frame));

					if ($doMerge === 'yes') {
						$value1 = call_user_func_array($applyFunction, $mergeParams);
						$value1 = $parser->replaceVariables(ParserPower::unescape(trim($value1)), $frame);
						$matchParams[$valueIndex1] = $mergeParams[$valueIndex1] = $value1;
					} else {
						$preValues[] = $value2;
					}
				}

				$postValues[] = $value1;
			}
			$postCount = count($postValues);
			$preValues = $postValues;
		} while ($postCount < $preCount && $postCount > 1);

		return $postValues;
	}

	/**
	 * This function renders the listmerge function, sending it to the appropriate processing function based on what
	 * parameter values are provided.
	 *
	 * @param Parser  $parser The parser object.
	 * @param PPFrame $frame  The parser frame object.
	 * @param array   $params The parameters and values together, not yet expanded or trimmed.
	 *
	 * @return string|array The function output along with relevant parser options.
	 */
	public static function listmergeRender($parser, $frame, $params) {
		$params = ParserPower::arrangeParams($frame, $params);

		$inList = isset($params["list"]) ? trim($frame->expand($params["list"])) : '';
		if ($inList === '') {
			return isset($params["default"]) ? [ ParserPower::unescape(trim($frame->expand($params["default"]))),
				'noparse' => false
			] : '';
		}

		$inSep = isset($params["insep"]) ? ParserPower::unescape(trim($frame->expand($params["insep"]))) : ',';

		$inSep = $parser->mStripState->unstripNoWiki($inSep);

		$values = self::arrayTrimFilterUnescape(self::explodeList($inSep, $inList));
		if (count($values) === 0) {
			return isset($params["default"]) ? [ ParserPower::unescape(trim($frame->expand($params["default"]))),
				'noparse' => false
			] : '';
		}

		$matchTemplate = isset($params["matchtemplate"]) ? trim($frame->expand($params["matchtemplate"])) : '';
		$mergeTemplate = isset($params["mergetemplate"]) ? trim($frame->expand($params["mergetemplate"])) : '';
		$fieldSep = isset($params["fieldsep"]) ? ParserPower::unescape(trim($frame->expand($params["fieldsep"]))) : '';
		$sortMode = isset($params["sortmode"]) ? strtolower(trim($frame->expand($params["sortmode"]))) : 'nosort';
		$sortOptions = isset($params["sortoptions"]) ? trim($frame->expand($params["sortoptions"])) : '';

		if ($sortMode === 'presort' || $sortMode === 'pre/postsort') {
			$values = self::sortList($values, $sortOptions);
		}

		if ($matchTemplate !== '' && $mergeTemplate !== '') {
			$matchParams = [$parser, $frame, null, null, $matchTemplate, $fieldSep];
			$mergeParams = [$parser, $frame, null, null, $mergeTemplate, $fieldSep];
			$values = self::iterativeListMerge(
				$parser,
				$frame,
				$values,
				'ParserPower\\ParserPowerLists::applyTemplateToTwoValues',
				$matchParams,
				$mergeParams,
				2,
				3
			);
		} else {
			$token1 = isset($params["token1"]) ? ParserPower::unescape(trim($frame->expand($params["token1"]))) : '';
			$token2 = isset($params["token2"]) ? ParserPower::unescape(trim($frame->expand($params["token2"]))) : '';
			$tokenSep = isset($params["tokensep"]) ?
				ParserPower::unescape(trim($frame->expand($params["tokensep"]))) : ',';
			$matchPattern = isset($params["matchpattern"]) ? $params["matchpattern"] : '';
			$mergePattern = isset($params["mergepattern"]) ? $params["mergepattern"] : '';

			if ($tokenSep !== '') {
				$tokens1 = self::arrayTrimUnescape(explode($tokenSep, $token1));
				$tokens2 = self::arrayTrimUnescape(explode($tokenSep, $token2));
			} else {
				$tokens1 = [$token1];
				$tokens2 = [$token2];
			}
	
			$matchParams = [$parser, $frame, null, null, $fieldSep, $tokens1, $tokens2, $matchPattern];
			$mergeParams = [$parser, $frame, null, null, $fieldSep, $tokens1, $tokens2, $mergePattern];
			$values = self::iterativeListMerge(
				$parser,
				$frame,
				$values,
				'ParserPower\\ParserPowerLists::applyTwoSetFieldPattern',
				$matchParams,
				$mergeParams,
				2,
				3
			);
		}
		if (count($values) === 0) {
			return isset($params["default"]) ? [ ParserPower::unescape(trim($frame->expand($params["default"]))),
				'noparse' => false
			] : '';
		}

		if ($sortMode === 'sort' || $sortMode === 'postsort' || $sortMode === 'pre/postsort') {
			$values = self::sortList($values, $sortOptions);
		}

		$outSep = isset($params["outsep"]) ? ParserPower::unescape(trim($frame->expand($params["outsep"]))) : ', ';
		$countToken = isset($params["counttoken"]) ?
			ParserPower::unescape(trim($frame->expand($params["counttoken"]))) : '';
		$intro = isset($params["intro"]) ? ParserPower::unescape(trim($frame->expand($params["intro"]))) : '';
		$outro = isset($params["outro"]) ? ParserPower::unescape(trim($frame->expand($params["outro"]))) : '';

		$outList = implode($outSep, $values);
		$count = count($values);
		return [self::applyIntroAndOutro($intro, $outList, $outro, $countToken, $count), 'noparse' => false];
	}
}
